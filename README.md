# OS and Linux Intro Class

This class we'll look at both OS systems and Linux

### OS 

OS stands for Operating System. They are a level of abstraction on the bare metal.

Some have GUI- grafiucal user interface such as Android, Microsoft, MacOS, and lots of Linux distribution.

Others do not.

Some are paid, others are not.

We will be using Linux a lot

### Linux 

There are many Linux distributions. Here are some:

- Ubuntu (debian)(user friendly and widely used)
- Redhat: Centos 
- Arch
- Kali (security testing/penetration testing/ ethical hacking)

#### Bash Shells 
Linux come with Bash terminals and they might have different shells 

Different shells behave slightly differently.
- Bash
- gitbash 
- oh my zs-h 
- other 


#### Package Managers

Different distributions have different package managers, these help install software such as Python or nginx and apache.


#### Aspects

Everything is a file in Linux which means that your keyboard and monitor is a file- you can read the output and pass that output to other files. 

Everything can interact with everything which is great for automation.


#### Enviornment variables 


### 2 important paths '/' and '~'
/ means root directory 
~ means 'users/your_login_user/'

Enviornment is a place where code runs and in Bash we might want to set some variables. 
Variable is like a box, you have a name and you add stuff to it. You can open it later. 

In Bash we define a variable simply by:



# Setting a variable 
MY_VAR="This is a VAR in my code"

# Call the variable using echo $
echo $MY_VAR

# You can reassign the variable 


#### Setting enviornment variables 






### Child process 

A process that is initiated under other another process. Like running another Bash script.

It goes via the $PATH but is a new "terminal"
Hence if we:


BASH

# 1) set variable in terminal
LANDLORD="Jess"

# run the bash script that has echo $LANDLORD
./bash_file.sh
> hi from the file
> 
> Landlord above^^^

# This happens because the ./bash_file.sh runs as a child process - A new terminal



#### <u>3 file descriptors</u>
To interact with programmes and our files, we can use redirect and play around with 0, 1 and 2 that are the STDIN, SDOUT and STDERR.

**0 Standard input (stdin)**

**1 Standard output (stdout)**

**2 Standard error (stderr)**

### Path
```
Terminal & Bash process follow the PATH

There is a PATH for login users that have a profile and the ones without.

Files to consider 

```

### Common Commands 


### Permissions 

### Changing permissions 
$ chmod + rwd <files>
### Users

$ ENV - to check variables 

## Wildcards

- A wildcard in Linux is a symbol or a set of symbols that stands in for other characters.
- The Bash Wildcards are characters with special meanings when used for Pattern Matching.
```
Examples of these would be:
- $ cd /usr/bin; ls ??a??
- $ file /usr/bin/*x*

Setting a variable
MY_VAR="This is a VAR in my code"

Call the variable using echo
echo $MY_VAR
>"This is a VAR in my code"

You can re assign the variable 
MY_VAR="Hello"
echo $MY_VAR
> hello

Call other variables the same way 
echo $USER
> Nicole 
```

## Piping and Redirecting 

- A pipe is a form of redirection (transfer of standard output to some other destination) that is used in Linux and other Unix-like operating systems to send the output of one command/program/process to another command/program/process for further processing.
- You can make it do so by using the pipe character '|'

- A pipeline may link as many commands together as you need to construct what you are trying to achieve.
```
An example of this would be": $ ls -l /etc | grep 'd' | grep '.......r.x' | wc -l

```
The above command will list all visible directories in /etc that are accessible by the world and tell us how many there are.

After runing this example command, my output was 0 as I do not have any directories visible to the world.

##### Redirecting -
```bash
# initially when cat example is run returned will be
$ cat example
> Hello
> my name
> is Nicole
# First example will use just >
echo 'Hi Nicole' > example
> cat example
> Hi Nicole
## The initial text has been replaced
# Second example using >>
echo 'Hi Nicole' >> example
cat example
> Hello
> my name
> is Nicole
> Hi Nicole
## The inital text has been appended to
```


### Grep
Grep is a command-line tool that allows you to find a string in a file or stream. It can be used with a regular expression to be more flexible at finding strings.


